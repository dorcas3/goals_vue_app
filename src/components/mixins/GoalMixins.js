export const Goal = {
    data() {
        return {
            goals: [],
            message: "",
        };
    },

    watch: {
        goals: function () {
            this.message = "Goals Added successfully";
        },
    },
    computed: {
        goal_count: function () {
            return " You have " + this.goals.length + " Goals added!";
        },
    },
}