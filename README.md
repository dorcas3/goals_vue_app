# Goal App

## Project Description

- An application demonstrates each of these items in the UIthe use of data function, computed properties, watchers, props, components, mixins, methods 

## Live link

https://focused-payne-0af3a7.netlify.app/
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

